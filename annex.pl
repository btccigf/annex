#!/usr/bin/perl
#
#  (c) 2018 https://gitlab.com/mrigf
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>

use strict;
use lib "lib"; 
use Getopt::Long;
use File::Temp qw(tempfile tempdir);
use File::Path qw(remove_tree);
use File::Copy;
use File::Basename;
use Cwd 'abs_path';
#use ODF::lpOD;
#{ package ODF::lpOD::Element; sub DESTROY {};  }

# binary required
my $bin_libreoffice = "/usr/bin/libreoffice";
my $bin_pdftk = "/usr/bin/pdftk";
my $bin_convert = "/usr/bin/convert";
my $bin_gs = "/usr/bin/gs";
my $bin_unzip = "/usr/bin/unzip";
my $bin_zip = "/usr/bin/zip";
for ($bin_gs, $bin_libreoffice, $bin_pdftk, $bin_convert, $bin_unzip, $bin_zip) {  die "unable to run $_, exiting " unless -x $_; }


# command line args
my ($help,$getopt,$debug,$frontpage,$firstpagestatement);
my $dir = ".";
eval {
    $getopt = GetOptions("debug" => \$debug,
			 "help" => \$help,
			 "dir=s" => \$dir,
			 "front-page=s" => \$frontpage,
			 "first-page-statement=s" => \$firstpagestatement,
	);
};


# print help
if ($help) {

    print "Usage: $0 [OPTIONS]

      --dir=dir        Directory containing files to annex and number
                       (by default: current directory)
      --front-page=file.odt     
                       Front page, non numbered                         
      --first-page-statement=statement
                       Extra statement to put on the front page along
                       numbering
";
    
}

# get dir full path and defines output name
$dir = abs_path($dir);
my $final_pdf = $dir."/".basename($dir)."-ANNEXES.pdf";

my $unlink = 1;
$unlink = 0 if $debug;

# test access to dir
chdir($dir) or die "unable to enter $dir, exiting";



# Copy relevant files to tmpdir, convert to PDF
my ($dir_full_pdf) = tempdir(UNLINK => $unlink);
my $libreoffice_types = "odg|odt|ods|doc|docx|xls|xlsx|txt|png|jpg|jpeg"; 
while (defined(my $file = glob("*"))) {
    print "$file\n" if $debug;
    # skip directories
    next if -d $file;
    
    # skip unreadable
    next unless -r $file;
    
    # skip if matching output file
    next if $file eq $final_pdf;
    
    # PDF can be directly copied as they are
    if (lc($file) =~ /\.pdf$/i) {
	print "$file -> $dir_full_pdf/\n" if $debug;
	copy($file, $dir_full_pdf);
    }
	
    # go through libreoffice valid types
    if (lc($file) =~ /\.($libreoffice_types)$/i) {
	print "$file -> $dir_full_pdf/\n" if $debug;
	system($bin_libreoffice,
	       "--convert-to", "pdf",
	       "--outdir", $dir_full_pdf,
	       $file);	
    }
    
}

# first pass, burst into multiple files 
my ($dir_splitted_pdf) = tempdir(UNLINK => $unlink);
my $filecount = 1;
while (defined(my $file = glob($dir_full_pdf."/*"))) {  
    # skip directories
    next if -d $file;   
    # skip unreadable
    next unless -r $file;
    # ignore if not PDF
    next unless lc($file) =~ /\.pdf$/i;

    # split files
    print "$file -> $dir_splitted_pdf/".sprintf("%08d", $filecount)."_%08d.pdf\n" if $debug;
    system($bin_pdftk,
	   $file,
	   "burst",
	   "output",
	   $dir_splitted_pdf."/".sprintf("%08d", $filecount)."_%08d.pdf");
    $filecount++;
}

# second pass, build numbered pages
my ($dir_numbered_splitted_pdf) = tempdir(UNLINK => $unlink);
my $page_count = 0;
while (defined(my $file = glob($dir_splitted_pdf."/*"))) {
    # skip directories
    next if -d $file;   
    # skip unreadable
    next unless -r $file;
    # ignore if not PDF
    next unless lc($file) =~ /\.pdf$/i;

    # increment count
    $page_count++;
    print "$file is page $page_count\n" if $debug;
    my $caption = $page_count;
    $caption = "$firstpagestatement\n\n$page_count" if $page_count == 1 and $firstpagestatement;

    
    # build a PDF with imagemagick
    # (latex would be more efficient but quite a big dependancy)
    # problem is the text is a bitmap 
    # https://superuser.com/questions/479652/vectorized-fonts-in-pdf-output-of-imagemagick-montage
    my ($numbered_a4h, $numbered_a4) = tempfile(SUFFIX => ".pdf", UNLINK => $unlink);
    system($bin_convert,
     	   "-page", "a4",
	   "canvas:transparent",
	   $numbered_a4);
    
    my ($numbered_texth, $numbered_text) = tempfile(SUFFIX => ".pdf", UNLINK => $unlink);
    system($bin_convert,
	   "-background", "transparent",
	   "-bordercolor", "transparent",
	   "-border", "20x20",
	   "-pointsize", "9", 
	   "+antialias", # less ugly but far from good
	   "-font", "/usr/share/fonts/truetype/liberation/LiberationMono-Regular.ttf",
	   "label:".$caption,
	   $numbered_text);
    
    my ($numberedh, $numbered) = tempfile(SUFFIX => ".pdf", UNLINK => $unlink);
    system($bin_convert,
	   $numbered_a4,
	   "-coalesce",
	   "-gravity", "SouthWest",
	   "null:",
	   $numbered_text,
	   "-layers", "composite",
	   $numbered);


    ### Another approach with imagemagick, without intermediary images
    ### (but line breaks are not good)
    #
    # my ($numberedh, $numbered) = tempfile(SUFFIX => ".pdf", UNLINK => $unlink);
    # system($bin_convert,
    #  	   "-page", "a4",
    #  	   "-background", "transparent",
    #  	   "-bordercolor", "transparent",
    #  	   "-gravity", "SouthWest", # to the left, right might already have sigs
    # 	   "-pointsize", "1",
    #  	   "-units", "PixelsPerInch",
    #  	   "-density", "600",
    #  	   "-border", "20x20", 
    # 	   "caption:$caption", # ugly and non text with caption, misplaced
    #  	                       # and duplicated with label
    #  	   $numbered);

    ### build a PDF with libreoffice (would be more efficient with LaTeX
    ### but limit dependancies)
    # my ($dir_numbered) = tempdir(UNLINK => $unlink);
    # my $numbered = "$dir_numbered/$page_count.pdf";
    # my $numbered_odt = "$dir_numbered/$page_count.odt";

    # print "build $numbered_odt with caption $caption\n" if $debug;
    # my $numbered_odf_build = odf_document->create('text');
    # $numbered_odf_build->body->insert_element(odf_paragraph->create(text => $caption));
    # $numbered_odf_build->save(target => $numbered_odt);
    # die "failed to write $numbered_odt" unless -e $numbered_odt;
    
    # print "$numbered_odt -> $numbered\n" if $debug;
    # system($bin_libreoffice,
    # 	   "--convert-to", "pdf",
    # 	   "--outdir", $dir_numbered,
    # 	   $numbered_odt);
    # die "failed to write $numbered" unless -e $numbered;
    
    # Mix the number and the source pdf
    print "+ $numbered -> ".$dir_numbered_splitted_pdf."/page".sprintf("%08d", $page_count).".pdf\n" if $debug;
    system($bin_pdftk,
	   $numbered,
	   "background", $file,
	   "output", $dir_numbered_splitted_pdf."/page".sprintf("%08d", $page_count).".pdf");
}

# (optional) Third step, build and add front page
if ($frontpage) { 

    # copy front page in end pdf dir
    my $this_frontpage = "page000000000000000000000.odt";
    copy($frontpage, "$dir_numbered_splitted_pdf/$this_frontpage");
    
    # unzip odt
    my ($dir_frontpage_odt) = tempdir(UNLINK => $unlink);
    print "decompress front page $frontpage in $dir_frontpage_odt\n" if $debug;
    system($bin_unzip,
	   $frontpage,
	   "-d", $dir_frontpage_odt);

    # update content.xml
    open(NEW_FRONTPAGE, "> $dir_numbered_splitted_pdf/content.xml");
    open(FRONTPAGE, "< $dir_frontpage_odt/content.xml");
    while(<FRONTPAGE>) {
	s/XXX/$page_count/g;
	print NEW_FRONTPAGE $_;
    }
    close(FRONTPAGE);
    close(NEW_FRONTPAGE);

    # update the odt
    # FIXME NON PORTABLE
    `cd $dir_numbered_splitted_pdf && zip --update $this_frontpage content.xml`;
    print "cd $dir_numbered_splitted_pdf && zip --update $this_frontpage content.xml\n" if $debug;
       
    print "convert to PDF $dir_numbered_splitted_pdf/$this_frontpage\n" if $debug;
    system($bin_libreoffice,
	   "--convert-to", "pdf",
	   "--outdir", $dir_numbered_splitted_pdf,
	   "$dir_numbered_splitted_pdf/$this_frontpage"); 
}

# Merge into final PDF
print "$dir_numbered_splitted_pdf/page*.pdf > $final_pdf\n" if $debug;
# we actually want shell expansion here, hence specific call to system
#system("$bin_gs -q -sDEVICE=pdfwrite -dNOPAUSE -dBATCH -sOutputFile=$final_pdf -f $dir_numbered_splitted_pdf/page*.pdf"); # fails: * Error: ICCbased space /N value does not match the ICC profile.
system("$bin_pdftk $dir_numbered_splitted_pdf/page*.pdf cat output $final_pdf");

# Remove tmpdir
for ($dir_full_pdf, $dir_splitted_pdf, $dir_numbered_splitted_pdf) {
    die("$_ is a symlink instead of a directory, possible tampering!") if -l $_;
    die("$_ should be a directory, possible tampering") unless -d $_; 
    remove_tree($_) or print "failed to remove $_\n" unless $debug;
}


# EOF
